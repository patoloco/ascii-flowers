// eslint-disable-next-line
const colors = require('colors');

const { grass } = require('../grass');

const { flip, rnd } = require('../utils');

const { draw } = require('../draw');

const { MIN_INTERVAL, MAX_INTERVAL } = require('../config');

function sceneColor(interval, totalTime, x = null) {
  let g = grass(3, 32, false);

  const timer = setInterval(() => {
    console.clear();

    if (x) {
      const top = x(2);
      draw(top);
    }

    const arr = [];
    for (let i = 0, len = g.length; i < len; i++) {
      const splitLine = g[i].split('');

      // check whether to shift the chars around
      if (rnd(50)) {
        if (rnd(75)) {
          const popped = splitLine.pop();
          splitLine.unshift(popped);
        } else {
          const shifted = splitLine.shift();
          splitLine.push(shifted);
        }
      }

      // color the chars
      const coloredLine = [];
      splitLine.forEach((char) => {
        if (rnd(50)) {
          coloredLine.push(char);
        } else {
          coloredLine.push(char.green);
        }
      });
      arr.push(coloredLine.join(''));
    }

    draw(arr);
  }, interval);

  setTimeout(() => {
    clearInterval(timer);
  }, totalTime);
}

function scene(interval, totalTime, x = null) {
  if (interval < MIN_INTERVAL || interval > MAX_INTERVAL) {
    throw new Error(`interval must be ${MIN_INTERVAL} to ${MAX_INTERVAL}`);
  } else if (x !== null && typeof x !== 'function') {
    throw new Error('x must be null or a function');
  }

  let g = grass(3, 32, false);

  const timer = setInterval(() => {
    console.clear();

    if (x) {
      const top = x(2);
      draw(top);
    }

    const arr = [];
    for (let i = 0, len = g.length; i < len; i++) {
      let tmp;
      const newChar = flip('^', '~');

      if (rnd(50)) {
        tmp = g[i].substr(0,31);
        arr.push(newChar + tmp);
      } else {
        tmp = g[i].substr(1,32);
        arr.push(tmp + newChar);
      }
    }

    draw(arr);
  }, interval);

  setTimeout(() => {
    clearInterval(timer);
  }, totalTime);
}

module.exports = {
  scene,
  sceneColor,
};

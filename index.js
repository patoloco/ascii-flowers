const { air } = require('./air');
const { grass } = require('./grass');
const { dandelion, seven, tulip } = require('./flowers');

const { barn, house } = require('./buildings');

const { ant, movingAnts } = require('./creatures');

const { truck } = require('./items');

const { draw } = require('./draw');
const { scene, sceneColor } = require('./scene');

const { drawAsync, sceneAsync } = require('./async');

const config = require('./config');

module.exports = {
  air,
  ant,
  barn,
  config,
  dandelion,
  draw,
  drawAsync,
  grass,
  house,
  movingAnts,
  scene,
  sceneAsync,
  sceneColor,
  seven,
  truck,
  tulip,
};

const {
  MIN_HEIGHT_AIR,
  MAX_HEIGHT_AIR,
  MIN_LENGTH_AIR,
  MAX_LENGTH,
} = require('../config');

function air (height, length) {
  if (height < MIN_HEIGHT_AIR || height > MAX_HEIGHT_AIR) {
    throw new Error(`height can be ${MIN_HEIGHT_AIR} to ${MAX_HEIGHT_AIR}`);
  } else if (length < MIN_LENGTH_AIR || length > MAX_LENGTH) {
    throw new Error(`length can be ${MIN_LENGTH_AIR} to ${MAX_LENGTH}`);
  }

  const arr = [];
  for (let h = 0; h < height; h++) {
    let tmp = '';
    for (let i = 0; i < length; i++) {
      tmp += ' ';
    }
    arr.push(tmp);
  }

  return arr;
}

module.exports = {
  air,
};

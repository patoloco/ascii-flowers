const { draw } = require('../draw');
const { mix } = require('../utils');

const { MIN_INTERVAL, MAX_INTERVAL } = require('../config');

const _ant = [
  ' \\_/  ',
  '\'-0-\' ',
  '--0-- ',
  '.-0-. ',
];

function ant(x = 1) {
  if (x < 1 || x > 5) {
    throw new Error('x can be 1 to 5');
  }
  x = Math.floor(x);

  const tmp = ['', '', '', ''];
  for (let i = 0; i < x; i++) {
    tmp[0] += _ant[0];
    tmp[1] += _ant[1];
    tmp[2] += _ant[2];
    tmp[3] += _ant[3];
  }

  return tmp;
}

function movingAnts(interval, totalTime) {
  if (interval < MIN_INTERVAL || interval > MAX_INTERVAL) {
    throw new Error(`interval can be ${MIN_INTERVAL} to ${MAX_INTERVAL}`);
  }

  const space6 = '      ';

  const a = ant();
  a.push(space6);
  a.push(space6);
  const b = ant();
  b.push(space6);
  const c = ant();

  const ab = mix(a, b);
  const floor = mix(ab, c);

  const timer = setInterval(() => {
    console.clear();
    draw(floor);

    const shifted = floor.shift();
    floor.push(shifted);
  }, interval);

  setTimeout(() => {
    clearInterval(timer);
  }, totalTime);
}

module.exports = {
  ant,
  movingAnts,
};

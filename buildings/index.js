const barnFive = [
  '           x  ',
  '.-. _______|  ',
  '|=|/     /  \\ ',
  '| |_____|_""_|',
  '|_|_[X]_|____|',
];

function barn() {
  return barnFive;
}

const houseSix = [
  '  ____||____  ',
  ' ///////////\\ ',
  '///////////  \\',
  '|    _    |  |',
  '|[] | | []|[]|',
  '|   | |   |  |',
];

function house() {
  return houseSix;
}

module.exports = {
  barn,
  house,
};

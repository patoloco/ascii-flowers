const _truck = [
'     ____          ',
'____//_]|________  ',
'(o _ |  -|   _  o| ',
'`(_)-------(_)--\'  ',
];

function truck() {
  return _truck;
}

module.exports = {
  truck,
};

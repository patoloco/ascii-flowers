const { flip } = require('../utils');

const {
  MIN_HEIGHT_GRASS,
  MAX_HEIGHT_GRASS,
  MIN_LENGTH_GRASS,
  MAX_LENGTH,
} = require('../config');

function grass(height, length, even = true) {
  if (height < MIN_HEIGHT_GRASS || height > MAX_HEIGHT_GRASS) {
    throw new Error(`height can be ${MIN_HEIGHT_GRASS} to ${MAX_HEIGHT_GRASS}`);
  } else if (length < MIN_LENGTH_GRASS || length > MAX_LENGTH) {
    throw new Error(`length can be ${MIN_LENGTH_GRASS} to ${MAX_LENGTH}`);
  }
  height = Math.floor(height);
  length = Math.floor(length);

  const arr = [];

  for (let j = 0; j < height; j++) {
    let str = '';
    for (let k = 0; k < length; k++) {
      let tmp;
      if (!even) {
        tmp = flip('^', '~');
      } else {
        (k + j) % 2 === 1 ? tmp = '^' : tmp = '~';
      }

      str += tmp;

    }
    arr.push(str);
  }

  return arr;
}

module.exports = {
  grass,
};

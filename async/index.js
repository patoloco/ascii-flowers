const { grass } = require('../grass');

const { draw } = require('../draw');

const { flip, rnd } = require('../utils');

const { MIN_INTERVAL, MAX_INTERVAL } = require('../config');

function drawAsync(arr) {
  return new Promise((resolve, reject) => {
    draw(arr);
    resolve();
  });
}

function sceneAsync(interval, totalTime, x = null) {
  return new Promise((resolve, reject) => {
    if (interval < MIN_INTERVAL || interval > MAX_INTERVAL) {
      reject(new Error(`interval can be ${MIN_INTERVAL} to ${MAX_INTERVAL}`));
    } else if (x !== null && typeof x !== 'function') {
      reject(new Error('x must be a function or null'));
    } else {
      let g = grass(3, 32, false);

      const timer = setInterval(() => {
        console.clear();

        if (x) {
          const top = x(2);
          draw(top);
        }

        const arr = [];
        for (let i = 0, len = g.length; i < len; i++) {
          let tmp;
          const newChar = flip('^', '~');

          if (rnd(50)) {
            tmp = g[i].substr(0,31);
            arr.push(newChar + tmp);
          } else {
            tmp = g[i].substr(1,32);
            arr.push(tmp + newChar);
          }
        }

        draw(arr);
      }, interval);

      setTimeout(() => {
        clearInterval(timer);
        resolve();
      }, totalTime);
    }
  });
}

module.exports = {
  drawAsync,
  sceneAsync,
};

/* eslint no-unused-vars: 0 */
const { expect } = require('chai');

const { barn, house } = require('../buildings');

describe('Building', () => {
  describe('barn', () => {
    it('creates an array with length 5 and width 14', (done) => {
      const b = barn();
      expect(b).to.be.an('array').with.lengthOf(5);
      b.forEach((string) => {
        expect(string.length).to.equal(14);
      });
      done();
    });
  });

  describe('house', () => {
    it('creates an array with length 6 and width 14', (done) => {
      const h = house();
      expect(h).to.be.an('array').with.lengthOf(6);
      h.forEach((string) => {
        expect(string.length).to.equal(14);
      });
      done();
    });
  });
});

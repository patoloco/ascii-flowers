/* eslint no-unused-vars: 0 */
const { expect } = require('chai');
const sinon = require('sinon');

const sandbox = sinon.createSandbox();

const { ant, movingAnts } = require('../creatures');

describe('Creatures', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('ant', () => {
    it('ant() creates an array of length 4 and width 6', (done) => {
      const a = ant();
      expect(a).to.be.an('array').with.lengthOf(4);
      a.forEach((string) => {
        expect(string.length).to.equal(6);
      });
      done();
    });

    it('ant(n) creates an array of length 4 and width 6n', (done) => {
      for (let i = 0; i < 100; i++) {
        const n = Math.floor(Math.random() * 4) + 1;
        const a = ant(n);
        expect(a).to.be.an('array').with.lengthOf(4);
        a.forEach((string) => {
          expect(string.length).to.equal(6 * n);
        });
      }
      done();
    });
  });

  describe('movingAnts', () => {
    it('calls console.clear and .log the correct number of times', (done) => {
      const consoleClearStub = sandbox.stub(console, 'clear');
      const consoleLogStub = sandbox.stub(console, 'log');

      const interval = 250;
      const totalTime = 1600;

      movingAnts(interval, totalTime);

      const expectedConsoleClearCalls = Math.floor(totalTime / interval);
      const expectedConsoleLogCalls = Math.floor(totalTime / interval) * 6;

      setTimeout(() => {
        sinon.assert.callCount(consoleClearStub, expectedConsoleClearCalls);
        sinon.assert.callCount(consoleLogStub, expectedConsoleLogCalls);
        done();
      }, totalTime + interval);
    });
  });
});

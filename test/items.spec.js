/* eslint no-unused-vars: 0 */
const { expect } = require('chai');

const { truck } = require('../items');

describe('Items', () => {
  describe('truck', () => {
    it('returns an array with length 4 and width 19', (done) => {
      const t = truck();
      expect(t).to.be.an('array').with.lengthOf(4);
      t.forEach((string) => {
        expect(string.length).to.equal(19);
      });
      done();
    });
  });
});

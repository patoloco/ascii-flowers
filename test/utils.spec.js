const { expect } = require('chai');
const { flip, mix, rnd } = require('../utils');

const iterations = 1000;

describe('Utils', () => {
  describe('flip()', () => {
    it('returns "a" or "b"', (done) => {
      for (let i = 0; i < iterations; i++) {
        const result = flip('a', 'b');
        expect(['a', 'b']).to.include(result);
      }
      done();
    });
  });

  describe('mix()', () => {
    it('combines elements at same index of nested array using " "', (done) => {
      const arr1 = [
        'abc',
        '123',
      ];
      const arr2 = [
        'def',
        '456',
      ];

      const result = mix(arr1, arr2);
      expect(result[0]).to.equal('abc def');
      expect(result[1]).to.equal('123 456');

      done();
    });
  });

  describe('rnd()', () => {
    it('returns true or false for a number from 1 to 99', (done) => {
      for (let i = 0; i < iterations; i++) {
        const result = rnd(50);
        expect([true, false]).to.include(result);
      }
      done();
    });

    it('always returns true with a value 100 or higher', (done) => {
      for (let i = 0; i < iterations; i++) {
        const result = rnd(100);
        expect(result).to.equal(true);
      }
      done();
    });

    it('always returns false with a value 0 or lower', (done) => {
      for (let i = 0; i < iterations; i++) {
        const result = rnd(0);
        expect(result).to.equal(false);
      }
      done();
    });
  });
});

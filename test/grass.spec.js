/* eslint no-unused-vars: 0 */
const { expect } = require('chai');

const { grass } = require('../grass');

const {
  MIN_HEIGHT_GRASS,
  MAX_HEIGHT_GRASS,
  MIN_LENGTH_GRASS,
  MAX_LENGTH,
} = require('../config');

describe('Grass', () => {
  describe('MIN and MAX limits', () => {
    it('throws an error if height is below MIN', (done) => {
      try {
        const g = new grass(MIN_HEIGHT_GRASS - 1, MIN_LENGTH_GRASS);
      } catch (e) {
        done();
      }
    });

    it('throws an error if height is above MAX', (done) => {
      try {
        const g = new grass(MAX_HEIGHT_GRASS + 1, MIN_LENGTH_GRASS);
      } catch (e) {
        done();
      }
    });

    it('throws an error if length is below MIN', (done) => {
      try {
        const g = new grass(MIN_HEIGHT_GRASS, MIN_LENGTH_GRASS - 1);
      } catch (e) {
        done();
      }
    });

    it('throws an error if length is above MAX', (done) => {
      try {
        const g = new grass(MIN_HEIGHT_GRASS, MAX_LENGTH + 1);
      } catch (e) {
        done();
      }
    });
  });

  describe('size verification', () => {
    it('generates an array of length HEIGHT with each element a string of length LENGTH', (done) => {
      const h = MAX_HEIGHT_GRASS;
      const l = MAX_LENGTH;
      const g = new grass(h, l);
      expect(g).to.be.an('array').with.lengthOf(h);
      expect(g[0]).to.be.a('string').with.lengthOf(l);
      done();
    });
  });

  describe('char verification', () => {
    it('generates strings with only chars "~" and "^"', (done) => {
      const h = MAX_HEIGHT_GRASS;
      const l = MAX_LENGTH;
      const g = new grass(h, l);

      g.forEach((string) => {
        const tmp = string.split('');
        tmp.forEach((char) => {
          expect(['~', '^']).to.include(char);
        });
      });

      done();
    });
  });
});

/* eslint no-unused-vars: 0 */
const { expect } = require('chai');

const { dandelion, seven, tulip } = require('../flowers');

describe('Flowers', () => {
  describe('Dandelion', () => {
    it('creates an array of length 8 and width 12', (done) => {
      const d = dandelion();
      expect(d).to.be.an('array').with.lengthOf(8);
      d.forEach((string) => {
        expect(string.length).to.equal(12);
      });
      done();
    });
  });

  describe('Seven', () => {
    it('creates an array of length 7', (done) => {
      const s = seven();
      expect(s).to.be.an('array').with.lengthOf(7);
      done();
    });
  });

  describe('Tulip', () => {
    it('creates an array of length 7 and width 14', (done) => {
      const t = tulip(1);
      expect(t).to.be.an('array').with.lengthOf(7);
      t.forEach((string) => {
        expect(string.length).to.equal(14);
      });
      done();
    });
  });
});

/* eslint no-unused-vars: 0 */
const { expect } = require('chai');
const sinon = require('sinon');

const sandbox = sinon.createSandbox();

const { drawAsync, sceneAsync } = require('../async');

const { MIN_INTERVAL, MAX_INTERVAL } = require('../config');

describe('Async methods', () => {
  afterEach(() => {
    sandbox.restore();
  });

  describe('drawAsync()', () => {
    it('draws and then resolves', async () => {
      const consoleLogStub = sandbox.stub(console, 'log');

      const test = [ 'abc', 'def', 'ghi'];

      await drawAsync(test);
      sinon.assert.callCount(consoleLogStub, 3);
    });
  });

  describe('sceneAsync()', () => {
    it('draws for an interval and then resolves', async () => {
      const consoleClearStub = sandbox.stub(console, 'clear');
      const consoleLogStub = sandbox.stub(console, 'log');
      const interval = 300;
      const totalTime = 1650;

      await sceneAsync(interval, totalTime);

      const totalLogCalls = Math.floor(totalTime / interval) * 3;

      // defaults to 3 calls per interval drawing grass
      const totalClearCalls = Math.floor(totalTime / interval);

      sinon.assert.callCount(consoleClearStub, totalClearCalls);
      sinon.assert.callCount(consoleLogStub, totalLogCalls);
    });

    it('rejects with an interval lower than MIN', (done) => {
      sceneAsync(MIN_INTERVAL - 1, 1000)
        .then(() => {
          expect.fail();
        }, (e) => {
          done();
        });
    });

    it('rejects with an interval greater than MAX', (done) => {
      sceneAsync(MAX_INTERVAL + 1, 1000)
        .then(() => {
          expect.fail();
        }, (e) => {
          done();
        });
    });

    it('rejects if third param is not null or a function', (done) => {
      sceneAsync(MIN_INTERVAL, 1000, 'test')
        .then(() => {
          expect.fail();
        }, (e) => {
          done();
        });
    });
  });
});

/* eslint no-unused-vars: 0 */
const { expect } = require('chai');

const { air } = require('../air');

const {
  MIN_HEIGHT_AIR,
  MAX_HEIGHT_AIR,
  MIN_LENGTH_AIR,
  MAX_LENGTH,
} = require('../config');

describe('Air', () => {
  describe('MIN and MAX limits', () => {
    it('throws an error if height is below MIN', (done) => {
      try {
        const a = new air(MIN_HEIGHT_AIR - 1, MIN_LENGTH_AIR);
      } catch (e) {
        done();
      }
    });

    it('throws an error if height is above MAX', (done) => {
      try {
        const a = new air(MAX_HEIGHT_AIR + 1, MIN_LENGTH_AIR);
      } catch (e) {
        done();
      }
    });

    it('throws an error if length is below MIN', (done) => {
      try {
        const a = new air(MIN_HEIGHT_AIR, MIN_LENGTH_AIR - 1);
      } catch (e) {
        done();
      }
    });

    it('throws an error if length is above MAX', (done) => {
      try {
        const a = new air(MIN_HEIGHT_AIR, MAX_LENGTH + 1);
      } catch (e) {
        done();
      }
    });
  });

  describe('size verification', () => {
    it('generates an array of length HEIGHT with each element a string of length LENGTH', (done) => {
      const h = MAX_HEIGHT_AIR;
      const l = MAX_LENGTH;
      const a = new air(h, l);
      expect(a).to.be.an('array').with.lengthOf(h);
      expect(a[0]).to.be.a('string').with.lengthOf(l);
      done();
    });
  });

  describe('char verification', () => {
    it('generates strings with only char " "', (done) => {
      const h = MAX_HEIGHT_AIR;
      const l = MAX_LENGTH;
      const a = new air(h, l);

      a.forEach((string) => {
        const tmp = string.split('');
        tmp.forEach((char) => {
          expect(char).to.equal(' ');
        });
      });

      done();
    });
  });
});

/* eslint no-unused-vars: 0 */
const { expect } = require('chai');
const sinon = require('sinon');

const sandbox = sinon.createSandbox();

const { scene } = require('../scene');

const { MIN_INTERVAL, MAX_INTERVAL } = require('../config');

describe('Scene', () => {
  afterEach(() => {
    sandbox.restore();
  });

  it('logs to the console n times in an interval i', (done) => {
    const consoleClearStub = sandbox.stub(console, 'clear');
    const consoleLogStub = sandbox.stub(console, 'log');

    const interval = 200;
    const totalTime = 1700;

    const s = scene(interval, totalTime);

    // scene default is 3 high grass
    const totalExpectedLogCalls = Math.floor(totalTime / interval) * 3;
    const totalExpectedClearCalls = Math.floor(totalTime / interval);

    setTimeout(() => {
      sinon.assert.callCount(consoleClearStub, totalExpectedClearCalls);
      sinon.assert.callCount(consoleLogStub, totalExpectedLogCalls);
      done();
    }, totalTime + interval);
  });

  it('throws an error if third param is not null or a function', (done) => {
    expect(function() { scene(200, 1700, 5) }).to.throw();
    done();
  });

  it('throws an error if interval is below MIN', (done) => {
    expect(function() { scene(MIN_INTERVAL - 1, 1000) }).to.throw();
    done();
  });

  it('throws an error if interval is above MAX', (done) => {
    expect(function() { scene(MAX_INTERVAL + 1, 1000) }).to.throw();
    done();
  });
});

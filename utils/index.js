const { mix } = require('./mix');

function flip(x, y) {
  return Math.random() > 0.5 ? x : y;
}

function rnd(n) {
  return Math.random() * 100 < n;
}

module.exports = {
  flip,
  mix,
  rnd,
};

function mix(a, b) {
  if (!a || !b || !Array.isArray(a) || !Array.isArray(b)) {
    throw new Error('a and b must be arrays');
  }

  const diff = a.length - b.length;

  if (diff > 0) {
    for (let i = 0; i < diff; i++) {
      b.unshift(' ');
    }
  } else if (diff < 0) {
    for (let i = 0, len = Math.abs(diff); i < len; i++) {
      a.unshift(' ');
    }
  }

  const tmp = [];

  for (let i = 0, len = a.length; i < len; i++) {
    const newLine = a[i] + ' ' + b[i];
    tmp.push(newLine);
  }

  return tmp;
}

module.exports = {
  mix,
};

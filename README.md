#ascii-flowers

##simple ascii art from around the web


###samples
#####npm run sampleOne
#####npm run sampleTwo
#####npm run sampleThree


###code example
const { draw, tulip } = require('ascii-flowers');

draw(tulip(2));

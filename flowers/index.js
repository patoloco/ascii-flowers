const { dandelion } = require('./dandelion');
const { seven } = require('./sevenHigh');
const { tulip } = require('./tulip');

module.exports = {
  dandelion,
  seven,
  tulip,
};

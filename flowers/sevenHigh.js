const sevenHigh = [
  '      wWWWw             wWWWw',
  'vVVVv (___) wWWWw       (___) vVVVv',
  '(___)  ~Y~  (___) vVVVv  ~Y~  (___)',
  ' ~Y~   \\|    ~Y~  (___)   |/   ~Y~',
  ' \\|    \\|/   \\|/  \\~Y~/  \\|    \\|/',
  '\\\\|// \\\\|// \\\\|///\\\\\|// \\\\|// \\\\\|///',
  '^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~^~',
];

function seven(x = 1, spacing = 0) {
  if (x < 1 || x > 6) {
    throw new Error('only 1 to 6 allowed');
  } else if (spacing < 0 || spacing > 2) {
    throw new Error('spacing can be 0, 1, or 2');
  }
  x = Math.floor(x);
  spacing = Math.floor(spacing);

  const tmp = ['', '', '', '', '', '', ''];

  for (let i = 0; i < x; i++) {
    for (let j = 0; j < 7; j++) {
      const start = i * 6;
      tmp[j] += sevenHigh[j].substr(start, 5);

      for (let k = 0; k < spacing; k++) {
        if (j === 6) {
          tmp[j] += '^';
        } else {
          tmp[j] += ' ';
        }
      }
    }
  }

  return tmp;
}

module.exports = {
  seven,
};

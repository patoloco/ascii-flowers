const _tulip = [
  '         __/) ',
  '      .-(__(=:',
  '   |\\ |    \\) ',
  '   \\ ||       ',
  '    \\||       ',
  '     \\|       ',
  '      |       ',
];

function tulip(x = 1) {
  if (x < 1 || x > 3) {
    throw new Error('only 1 to 3 allowed');
  }

  if (x === 1) {
    return _tulip;
  }

  const tmp = ['', '', '', '', '', '', ''];

  for (let i = 0; i < x; i++) {
    for (let j = 0; j < 7; j++) {
      tmp[j] += _tulip[j];
      if (i !== x - 1) {
        tmp[j] += ' ';
      }
    }
  }

  return tmp;
}

module.exports = {
  tulip,
};

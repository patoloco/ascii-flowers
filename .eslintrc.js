module.exports = {
    "env": {
        "es6": true,
        "mocha": true
    },
    "plugins": [ "mocha" ],
    "rules": {
        "no-multi-assign": 0,
        "no-plusplus": [2, { allowForLoopAfterthoughts: true }],
        "no-return-await": 0,
        "no-shadow": 1,
        "no-unused-vars": 1,
        "prefer-destructuring": 1,
        "prefer-template": 0,
        "import/prefer-default-export": false,
        "semi": [2, "always"]
    }
};
